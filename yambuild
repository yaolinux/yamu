#!/bin/bash
# YAMBUILD : 
# Created by : Yaolinux Team
# Created at : 30/04/2020
# Modified at : 08/04/2020

source_download() {
	_information $prefix "Téléchargement de la source"
	cd $build_arbo/$NAME/sources
	for i in "${URL[@]}";
	do
		NAMESOURCE=" ${i##*/}"
		if [ ! -e  ${NAMESOURCE} ]; then
			case $i in
				http://*|https://*|ftp://*)
					wget -c $i
					if [[ $? != 0 ]]; then
						_warning $prefix "Le certificat de la source n'est pas correct."
						wget -c --no-check-certificate $i
					fi
					;;
				git#*)
					GITSOURCE="${i##*#}"
					GITNAMESOURCE="${GITSOURCE##*/}"
					GITFOLDERNAME=$(echo $GITNAMESOURCE | cut -d "." -f1)
					_information $prefix "Clonage du dépôt"
					if [ -d $GITFOLDERNAME ]; then
						_warning $prefix "Nettoyage des anciennes sources"
						rm -r $GITFOLDERNAME
					fi
					git clone $GITSOURCE
					if [ $? != 0 ]; then
						_error $prefix "Impossible de cloner le dépôt"
						exit 17
					else
						cp -r $GITFOLDERNAME $build_dir
					fi
					;;
				* | ".*")
					_warning $prefix "Copie de la source ${NAMESOURCE} dans le dossier de compilation"
					cp -rf $basedir/$i $build_dir
					;;
			esac
		else
			_information $prefix "La source est déjà éxistante"
		fi
		SRCNAME+=" ${i##*/}"
	done
}

source_decompress() {
	_information $prefix "Décompression des sources"
	cd $build_arbo/$NAME/sources
	for tarball in ${SRCNAME[@]};
	do
		case $tarball in
			*.tar|*.tar.gz|*.tar.Z|*.tgz|*.tar.bz2|*.tbz2|*.tbz|*.tar.xz|*.txz|*.tar.lzma|*.tar.lz)
				tar xf $tarball -C $build_arbo/$NAME/build
				;;
			*)
				#_warning $prefix "Fichier ou archive ${tarball} non pris en charge"
				if [ $tarball == ".install" ]; then
					cp -rf $build_dir/$tarball $package_dir
				fi
				;;

		esac
	done
}

source_postdecompress() {
	_information $prefix "Vérification des sources"
	cd $build_arbo/$NAME/sources
	for tarball in $TAR;
	do
		folder_pkg=$(tar tf $tarball | head -1)
	done
}

check_dependencies(){
	_information $prefix "Vérification des dépendances"
	if [ ! -z $DEPENDS ]; then
		for dependencies in ${DEPENDS[@]}
		do
			if [ ! -d $PKG_DB/$dependencies ]; then
				_warning $prefix "Le paquet ${dependencies} n'est pas installé, Vérification dans le dépot"
				if [ ! -d $PKG_DEPOT/$dependencies ]; then
					_error $prefix "Le paquet ${dependencies} est inexistant"
					exit 15
				else
					_warning $prefix "Merci d'installer le dépendances ${dependencies}"
					exit 16
				fi
			fi
		done
	fi

}

find_runtime() {
	_information $prefix "Vérification des dépendances runtime"
	search=$(find $package_dir -type f \( -perm /+u+x -o -name '*.so' -o -name '*.so.*' \) 2>/dev/null)

	package=$PKG_DB/*

	for file in ${search[@]}
	        do
	                type="$(file -b $file | cut -d " " -f1)"
	                if [ $type == ELF ]; then
	                                dep="$(readelf -d ${file} | grep -i needed | cut -d ":" -f2 | cut -d " " -f2 | tr -d "[]")"
	                                runtimedep+="$(grep -i "${dep}" $package/.PACKTREE | cut -d "/" -f4 | sed -r '/^\s*$/d') "
	                fi
	                unset type
	        done

	sortedruntimedep=$(echo -e ${runtimedep[@]} | tr " " "\n" | sort -u)
}

package_pack() {
	_information $prefix "Empaquetage du paquet"
	cd $package_dir
	TARFILE=$NAME-$VERSION-$RELEASE.tar
	tar --no-same-owner -cf $NAME-$VERSION-$RELEASE.tar *
	if [ $? != 0 ]; then
		_error $prefix "Erreur sur l'empaquetage, vérifier la fonction do_install"
		exit 14
	fi
	_information $prefix "Création de l'arborescence du paquet"
	tar -tf $TARFILE > .PACKTREE
	tar -rf $TARFILE .PACKMETA
	tar -rf $TARFILE .PACKTREE
	if [ -f ".install" ]; then
		tar -rf $TARFILE .install
	fi
	_information $prefix "Création de l'archive du paquet"
	zstd -q --rm $TARFILE
}

package_meta() {
	cd $package_dir
	echo -e "$NAME,$VERSION,$RELEASE,"${sortedruntimedep[@]} > .PACKMETA
}

remove_la_file(){
	_information $prefix "Suppression des fichiers .la"
	find $package_dir ! -type d -name "*.la" -print -delete
}

strip_files() {
	_information $prefix "Lancement de la fonction strip"
	local FILE

	cd $package_dir

	find . -type f -printf "%P\n" | cat | while read FILE; do
		case $(file -b "$FILE") in
		*ELF*executable*not\ stripped)
			strip --strip-all "$FILE"
			;;
		*ELF*shared\ object*not\ stripped)
			strip --strip-unneeded "$FILE"
			;;
		current\ ar\ archive)
			strip --strip-debug "$FILE"
		esac
	done
}

package_move_depot() {
	_information $prefix "Déplacement du binaire dans le dépot local"
	if [ ! -d $PKG_DEPOT/$NAME ]; then
		mkdir $PKG_DEPOT/$NAME
	else
		rm -rf $PKG_DEPOT/$NAME/*
	fi
	mv -f $package_dir/$NAME-$VERSION-$RELEASE.tar.zst $PKG_DEPOT/$NAME
}

build_check() {
	_information $prefix "Vérification du fichier build"
	if [ -z $NAME ]; then _error $prefix "Variable NAME vide"; exit 100; fi
	if [ -z $VERSION ]; then _error $prefix "Variable VERSION vide"; exit 101; fi
	#[ -z $URL ] && _error $prefix "Variable URL vide"; exit 102
	if [ -z $RELEASE ]; then RELEASE=1; fi
	if [ -z $DEPENDS ]; then _information $prefix "Paquet sans dépendances"; fi
}


pkg_build() {
	build_init
	build_check
	check_dependencies
	source_download
	if [[ $? != 0 ]]; then
		_error $prefix "Merci de vérifier l'url de la source."
		exit 1
	fi
	source_decompress
	if [[ $? != 0 ]]; then
		_error $prefix "Merci de vérifier l'archive de la source."
		exit 2
	fi
	source_postdecompress
	if [[ $? != 0 ]]; then
		_error $prefix "Erreur sur la post décompression."
		exit 3
	fi
	cd $build_dir/
	if [[ $? != 0 ]]; then
		_error $prefix "Le dossier contentant les sources est inexistant."
		exit 4
	fi
	PACKDIR=$package_dir
	#Verification de l'existance de la fonction do_prepare dans la recette.
	if exist_function "do_prepare" ; then
		do_prepare
		if [[ $? != 0 ]]; then
			_error $prefix "Erreur sur la préparation de la recette. Merci de vérifier la fonction do_prepare."
			exit 5
		fi
	fi
	do_build
	if [[ $? != 0 ]]; then
		_error $prefix "Erreur sur la compilation de la recette. Merci de vérifier la fonction do_build."
		exit 6
	fi
	#Verification de l'existance de la fonction do_check dans la recette.
	if exist_function "do_check" ; then
		do_check
		if [[ $? != 0 ]]; then
                        _error $prefix "Erreur sur la préparation de la recette. Merci de vérifier la fonction do_prepare."
                        exit 5
                fi
        fi
	do_install
	if [[ $? != 0 ]]; then
		_error $prefix "Erreur sur l'installation du paquet. Merci de vérifier la fonction do_install."
		exit 7
	fi
	#Suppression des caractere de deboguage
	strip_files
	#Suppression des fichiers .la
	remove_la_file
	#Ajout des meta dans l'archives
	find_runtime
	package_meta
	if [[ $? != 0 ]]; then
		_error $prefix "Erreur sur la création du fichier .PACKMETA."
		exit 8
	fi
	#Empaquetage de le compilation
	package_pack
	if [[ $? != 0 ]]; then
		_error $prefix "Erreur sur l'empaquetage du paquet."
		exit 8
	fi
	#Mise en dépot du paquets
	package_move_depot
	if [[ $? != 0 ]]; then
		_error $prefix "Impossible de déplacer le binaire dans le dépôt. Merci de verifier les droits"
		exit 9
	fi

}

build_init() {
	_information $prefix "Verification de la présence des utilitaires obligatoires"
	fakeroot -v > /dev/null 2>&1
	if [ $? != 0 ]; then
		_error $prefix "Merci de verifier la présence de Fakeroot"
		exit 12
	fi
	_information $prefix "Chargement de la confiugration de YAMU"
	source yamu.conf
	if [ -z $MAKECORE ]; then
		NBCORE=$(nproc)
		export MAKEFLAGS="-j ${NBCORE}"
	else
		export MAKEFLAGS="-j ${MAKECORE}"
	fi
	_information $prefix "Vérification des droits"
	if [ ! -d /var/pkg ]; then
		_error $prefix "Le dossier /var/pkg n'existe pas, merci de le créer"
		exit 10
	elif [  ! -w /var/pkg ]; then
		_error $prefix "Le dossier /var/pkg n''est pas inscriptible, merci de vérifier les droits"
		exit 11
	fi

	_information $prefix "Suppression des anciens dossiers"
	if [ -d "${build_arbo}/${NAME}" ];
	then
		rm -rf $build_arbo/$NAME/{build,package}/*
	else

		_information $prefix "Création des dossiers necessaires à la compilation"

		mkdir -pv $build_arbo/$NAME/
		mkdir -pv $build_arbo/$NAME/sources
		mkdir -pv $build_arbo/$NAME/build
		mkdir -pv $build_arbo/$NAME/package
	fi
}

check_build_file() {
	_information $prefix "Verification du fichier build"
	exist_function "do_build"
	if [ $? != 0 ]; then
		_error $prefix "Le fonction do_build doit être présente"
		exit 13
	fi

}

exist_function() {
	declare -f "$1" > /dev/null
}

help() {
	echo -e "Aide pour yambuild :"
	echo -e "--download ou -do : Permet de télécharger les sources et initalise l'arborescence de compilation"
	echo -e "--decompress ou -de : Décompresser les sources"
	echo -e "--prepare ou -p : Lance la fonction prepare du build"
	echo -e "--make ou -m : Lance la fonction make du build"
	echo -e "--check ou -c : Lance la fonction check du build"
	echo -e "--install ou -i : Lance la fonction install du build"
}

prefix="YAMBUILD"

basedir=$PWD/$1
buildfile=$1/build
option=$2

source yamutils
source $buildfile

SRCNAME=""
PKG_DEPOT="/var/pkg"
PKG_DB="/var/db"
user_dir=$(echo $HOME)
build_arbo=$user_dir/yambuild
build_dir=$user_dir/yambuild/$NAME/build
package_dir=$user_dir/yambuild/$NAME/package

if [[ $user_dir == "/root" ]]; then
	_error $prefix "La compilation ne fait pas en root !"
	#exit 9
fi

case $2 in
"--download" | "-do")
	build_init
	source_download
	;;
"--decompress" | "-de")
	source_decompress
	source_postdecompress
	;;
"--prepare" | "-p")
	do_prepare
	;;
"--make" | "-m")
	do_make
	;;
"--check" | "-c")
	do_check
	;;
"--install" | "-i")
	PACKDIR=/$build_arbo/package
	export -f do_install
	fakeroot /bin/bash -c "do_install"
	;;
"--runtime" | "-r")
	find_runtime
	echo "test,test,"${sortedruntimedep[@]}
	;;
"--build" | "-b")
	pkg_build
	;;
*)
	pkg_build
	;;
esac
